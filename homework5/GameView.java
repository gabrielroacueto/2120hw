import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Transient;
import java.util.Random;
import java.util.EventListener;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.event.MouseAdapter;

/**
 *	Visual Implementation of Conway's Game of Life
 *	Gabriel Roa
 *	CSCI 2120 - Spring 2016
 */

public class GameView extends JPanel implements Observer{

	/** Instance of the Grid Model*/
	private Grid gridModel;

	/** Final Cell Size*/
	private static final int CELL_SIZE = 5;

	/**
	 *	Constructor Method for the Game View.
	 *
	 *	@param width The width in pixels of the GameView panel.
	 *	@param height The height in pixels of the GameView panel.
	 */
	public GameView(int width, int height){
		gridModel = new Grid(width / CELL_SIZE, height / CELL_SIZE);
		this.gridModel.addObserver(this);

		addKeyListener(new KeyAdapter(){	//Anonymous KeyAdapter to start event when b is pressed.
			public void keyTyped(KeyEvent e){
				if (e.getKeyChar() == 'b'){
					System.out.println("Simulation started.");
					new Timer(100, new ActionListener(){
						public void actionPerformed(ActionEvent e){
							gridModel.update();
						}
					}).start();
				}
			}
		});

		addMouseListener(new MouseAdapter(){	//Anonymous MouseAdapter to give live to cells when the mouse is clicked.
			public void mouseClicked(MouseEvent e){
				int cellX = e.getX() / CELL_SIZE;
				int cellY = e.getY() / CELL_SIZE;

				gridModel.setCellAlive(cellY, cellX, true);
			}
		});

	}

	/**
	 * Update method for Observer. Runs when the grid model changes state.
	 * 
	 * @param obs Observable object, which in this case will be the grid model.
	 * @param obj Object parameter, which is not used in this case.
	 */
	public void update(Observable obs, Object obj){
		if (obs == this.gridModel){
			this.repaint();
		}
	}

	/**
	* Dimension class to set an automatic size for the Panel.
	*/
	public Dimension getPreferredSize() {
        return new Dimension(750, 1000);
    }

    /**
    * Method that checks the grid model array and sees which cell is alive. Whenever it gets a life cell it paints it red.
    */
	public void paintComponent(Graphics g){
		super.paintComponent(g);

		for(int i = 0; i < gridModel.rowCount(); i++){ //i represents the Rows or the Y axis.
			for(int j = 0; j < gridModel.columnCount(); j++){ //j represents the X axis.
				if (gridModel.cellIsAlive(i,j)){
					g.setColor(Color.RED);
					g.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
				}
			}
		}
	}
}