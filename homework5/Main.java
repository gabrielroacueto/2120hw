import javax.swing.*;
import java.awt.*;
import java.util.*;
public class Main{
	public static void main(String[] args){
		GameView game = new GameView(800,1000);	//Creating Game View instance which is a JPanel class.
		game.setFocusable(true);			
		game.requestFocusInWindow();			//Methods to enable KeyAdapter functionality.
		JFrame frame = new JFrame();
		frame.setTitle("Conway's Game of Life");
		frame.getContentPane().add(game);		//Adding Panel to JFrame.
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		System.out.println("Setup game");
	}
}