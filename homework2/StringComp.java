import java.util.*;

/**
 * StringComp is a class that contains methods to either compare two Strings or two array
 * lists of the type string to see which one comes first in alphabetical order.
 * 
 * @author Gabriel Roa
 * @version 1.0
 * @since 1.0
 * 
 */
 
public class StringComp {
    
    public static int compareTo(String s1, String s2) {
        /**
         * Method for comparing two strings in alphabetical order.
         * 
         * @param s1    First string to be compared.
         * @param s2    Second string to be compared
         * @returns     String that comes first in alphabetical order.
         * @since 1.0
         */
         
        s1 = s1.trim().toLowerCase();
        s2 = s2.trim().toLowerCase();
        try {
            if (s1.charAt(0) > s2.charAt(0)) {
                return 1;
            } else if (s1.charAt(0) < s2.charAt(0)) {
                return -1;
            } else {
                
                if (s1.length() > 1 && s2.length() > 1)
                    return compareTo(s1.substring(1), s2.substring(1));
                else 
                    return 0;
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Comparing to an empty string.");
            return 2;
        }
    }
    
    public static String findMinimum(ArrayList<String> stringList) {
        /**
         * Method for comparing Strings in an array list of strings. The string that comes first in
         * alphabetical order will be returned.
         * 
         * @param stringList        An array list containing strings.
         * @returns                 The string that comes first in alphabetical order.
         * @since 1.0
         */
         
        
        int order = 0;
        
        order = compareTo(stringList.get(0),stringList.get(1));
        
        
        if (stringList.size() > 2) {    
        
            if (order == 1) {
                
                stringList.remove(0);
                
            } else {
                
                stringList.remove(1);
            }
            
        } else {
            
            return stringList.get(0); 
        }
        
        return findMinimum(stringList);
    }
}