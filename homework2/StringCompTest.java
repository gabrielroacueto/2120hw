import junit.framework.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Assert;
import java.util.*;
public class StringCompTest extends TestCase {
    
    public String s1;
    public String s2;
    public String s3;
    public String s4;
    public String s5;
    public String s6;
    
    ArrayList<String> stringList1;
    ArrayList<String> stringList2;
    ArrayList<String> stringList3;
    
    @Before
    public void setUp() {
        s1 = "This is a string a";
        s2 = "This is a string b";
        s3 = "Oneword";
        s4 = "Oneworda";
        s5 = "Onewordb";
        s6 = "";
        
        stringList1 = new ArrayList<String>();
        stringList2 = new ArrayList<String>();
        stringList3 = new ArrayList<String>();
        
        stringList1.add(s1);
        stringList1.add(s2);
        stringList1.add(s3);
        
        stringList2.add(s3);
        stringList2.add(s4);
        stringList2.add(s5);
        
        stringList3.add(s6);
        stringList3.add(s5);
        stringList3.add(s4);
    }
    
    public void testCompareTo() {
        assertEquals(StringComp.compareTo(s1, s2), -1);
        assertEquals(StringComp.compareTo(s4, s5), -1);
        assertEquals(StringComp.compareTo(s1, s3), 1);
        assertEquals(StringComp.compareTo(s6,s5), 2);
        assertEquals(StringComp.compareTo(s5,s4), 1);
    }
    
    public void testFindMinimum() {
        assertEquals(StringComp.findMinimum(stringList1), this.s1);
        assertEquals(StringComp.findMinimum(stringList2), this.s3);
        assertEquals(StringComp.findMinimum(stringList3), this.s6);
    }
}