/**
 * ComplexNumber is a class that will model a complex number based on two float
 * values given to it. This two float values will model the real and imaginary 
 * parts of a complex number and integrates basic addition, substraction,
 * multiplication and division for them.
 * 
 * @author Gabriel Roa
 * @version 1.0
 * @since 1.0
 */


public class ComplexNumber{
 
    private float a;
    private float b;
    
    public ComplexNumber(float a, float b){
        /**
         * Default constructor for a new complex number
         * 
         * @param a         real part.
         * @param b         imaginary part.
         * @since 1.0
         */
         
        this.a = a;
        this.b = b;
    }
    
    
    public float getA() {
        /**
         * Returns real part of the number
         * @return  real part a.
         */
         
        return this.a;
    }
    
    public float getB() {
        /**
         * Returns imaginary part of the number
         * @return      imaginary part b.
         */
         
        return this.b;
    }
    
    public ComplexNumber add(ComplexNumber otherNumber){
        
        /** 
         * The add() method implements addition for two complex numbers.
         * 
         * @param otherNumber   the number to be added to this number.
         * @return              a complex number that is the result of the addition.
         * @since 1.0
         */
         
        ComplexNumber newComplex;
        float newA = this.a + otherNumber.getA();
        float newB = this.b + otherNumber.getB();
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }
    
    public ComplexNumber subtract(ComplexNumber otherNumber){
        /**
         * The subtract() method implements subtraction for two complex numbers.
         * 
         * @param otherNumber       the number to be subtracted from this number.
         * @return                  a complex number result of the subtraction.
         * @since 1.0
         */
         
        ComplexNumber newComplex;
        float newA = this.a - otherNumber.getA();
        float newB = this.b - otherNumber.getB();
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }
    
    public ComplexNumber multiply(ComplexNumber otherNumber){
        /**
         * The multiply() method implements multiplication for two complex numbers
         * 
         * @param otherNumber       the number to be multiplied.
         * @return                  a complex number result of the multiplication.
         * @since 1.0
         */
         
        ComplexNumber newComplex;
        float newA = (this.a * otherNumber.getA()) - (this.b * otherNumber.getB());
        float newB = (this.b * otherNumber.getA()) + (this.a * otherNumber.getB());
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
        
    }
    
    public ComplexNumber divide(ComplexNumber otherNumber){
        /**
         * The divide() method implements division for two complexn numbers.
         * 
         * @param otherNumber        the number to divide this number.
         * @return                   a complex number result of the division.
         * @since 1.0
         */
         
        ComplexNumber newComplex;
        float newAnum = (this.a * otherNumber.getA()) + (this.b * otherNumber.getB());
        float newBnum = (this.b * otherNumber.getA()) - (this.a * otherNumber.getB());
        float newABden = ((float)Math.pow(otherNumber.getA(),2) + (float)Math.pow(otherNumber.getB(),2));
        
        float newA = newAnum / newABden;
        float newB = newBnum / newABden;
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }
    
    public boolean equals(ComplexNumber otherNumber){
        if (otherNumber.getA() == this.a && otherNumber.getB() == this.b){
            return true;
        } else {
            return false;
        }
    }
    
    public String toString(){
        
        String b = " + " + String.format("%.2f", this.b);
        
        if (this.b < 0) {
            b = " - " + String.format("%.2f", this.b*-1);
        }
        
        return String.format("%.2f%s%s",this.a,b,"i");
    }
}