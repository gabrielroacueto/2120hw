import junit.framework.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Assert;

public class ComplexNumberTest extends TestCase {
    public ComplexNumber firstNumber;
    public ComplexNumber secondNumber;
    public ComplexNumber thirdNumber;
    public ComplexNumber fourthNumber;
    public ComplexNumber fifthNumber;
    
    @Before 
    public void setUp(){
        firstNumber = new ComplexNumber(5,4);
        secondNumber = new ComplexNumber(3,2);
        thirdNumber = new ComplexNumber(5, 0);
        fourthNumber = new ComplexNumber(1,1);
        fifthNumber = new ComplexNumber(0,5);
    }
    
    @Test 
    public void testAdd(){
        assertEquals(firstNumber.add(secondNumber).toString(), "8.00 + 6.00i");
        assertEquals(secondNumber.add(thirdNumber).toString(), "8.00 + 2.00i");
        assertEquals(thirdNumber.add(fourthNumber).toString(), "6.00 + 1.00i");
        assertEquals(fifthNumber.add(thirdNumber).toString(), "5.00 + 5.00i");
    }
    
    public void testSubtract(){
        assertEquals(firstNumber.subtract(secondNumber).toString(), "2.00 + 2.00i");
        assertEquals(secondNumber.subtract(thirdNumber).toString(), "-2.00 + 2.00i");
        assertEquals(thirdNumber.subtract(fourthNumber).toString(), "4.00 - 1.00i");
        assertEquals(fourthNumber.subtract(fifthNumber).toString(), "1.00 - 4.00i");
    }
    
    public void testMultiply(){
        assertEquals(firstNumber.multiply(secondNumber).toString(), "7.00 + 22.00i");
        assertEquals(secondNumber.multiply(thirdNumber).toString(), "15.00 + 10.00i");
        assertEquals(thirdNumber.multiply(fourthNumber).toString(), "5.00 + 5.00i");
        assertEquals(fourthNumber.multiply(fifthNumber).toString(), "-5.00 + 5.00i");
    }
    
    public void testDivide(){
        assertEquals(firstNumber.divide(secondNumber).toString(), "1.77 + 0.15i");
        assertEquals(secondNumber.divide(thirdNumber).toString(), "0.60 + 0.40i");
        assertEquals(thirdNumber.divide(fourthNumber).toString(), "2.50 - 2.50i");
        assertEquals(fourthNumber.divide(fifthNumber).toString(), "0.20 - 0.20i");
    }
}