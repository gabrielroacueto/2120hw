public class LinkedList<T>  {


    Node<T> itsFirstNode;
    Node<T> itsLastNode;
    private int size;


    public LinkedList() {

        itsFirstNode = null;
        itsLastNode = null;
        size = 0;
    }

    
    public Iterator<T> getIterator() {
        return new Iterator(this);
    }

    // THIS WILL NEED TO BE MODIFIED FOR DOUBLY LINKED LIST
    public void add(T element) {

        Node<T> node = new Node(element);

        if (itsFirstNode == null) {
            itsFirstNode = node;
            itsLastNode = node;
        }
        else {
            itsLastNode.setNextNode(node);
            node.setPriorNode(itsLastNode);  //Setting the prior node of the added node at the end.
            itsLastNode = node;
            node.setNextNode(null);
        }
        size++;
    }

    // THIS WILL NEED TO BE MODIFIED FOR DOUBLY LINKED LIST
    public void add(T element, int index) {
        int counter = 0;
        Node<T> newNode = new Node(element);
        Node<T> current = itsFirstNode;
        while (current.getNextNode() != null ) {
            if (counter == index - 1 )
                break;
            else
                current = current.getNextNode();
                counter++;
        }

        newNode.setNextNode(current.getNextNode());
        current.getNextNode().setPriorNode(newNode); //Set current's next node's prior node to new node
        newNode.setPriorNode(current); //Setting prior node as current.
        current.setNextNode(newNode);
        size++;
    }

    public T get(int index) {

        int counter = 0;
        Node<T> current = itsFirstNode;
        while (current.getNextNode() != null ) {
            if (counter == index)
                break;
            current = current.getNextNode();
            counter++;
        }
        return current.getData();
    }

    // TO BE IMPLEMENTED
    
    // returns true if element is in the list, false if not
    public boolean contains(T element) {
        boolean contains = false;
        int counter = 0;
        Node<T> current = itsFirstNode;
        while(current.getNextNode() != null) {
            if (current.getData() == element) {
                contains = true;
                break;
            }
            current = current.getNextNode();
            counter++;
        }

        return contains;
    }

    // returns the index of the element if it is in the list, -1 if not found
    public int indexOf(T element) {
        int counter = 0;
        Node<T> current = itsFirstNode;
        while (current.getNextNode() != null) {
            if (current.getData().equals(element)) {
                return counter;
            } else {
                current = current.getNextNode();
                counter++;
            }
        }

        if (counter >= this.size - 1){
            counter = -1;
        }

        return counter;
    }

    // returns an Iterator at the location of the element if it is in the list
    // returns the null reference if the element is not found
    
    public Iterator<T> iteratorAt(T element) {
        Iterator iter = new Iterator(this);
        while (iter.hasNext()){
            if(iter.next().equals(element)){
                Object moveIter = iter.prior(); //Moving iter.
                return iter;
            }
        }

        return null;
    }
    

    public String toString() {
        String returnVal = "";
        Node<T> current = itsFirstNode;
        if (size != 0 ) {
            while (current.getNextNode() != null ) {
                returnVal += current.toString();
                returnVal += "\n";
                current = current.getNextNode();
            }

            returnVal += current.toString();
        }
        return returnVal;
    }  // end toString

    class Node<T> {
    
        private T data;
        private Node<T> itsNext;
        private Node<T> itsPrior;
    
        public Node(T data) {
            itsNext = null;
            itsPrior = null;
            this.data = data;
        }
    
    
        public T getData() {
            return this.data;
        }
    
        public Node<T> getNextNode() {
            return itsNext;
        }
        
        public Node<T> getPriorNode() {
            return itsPrior;
        }
        
    
        public void setNextNode(Node<T> next) {
            itsNext = next;
        }
        
        public void setPriorNode(Node<T> prior) {
            itsPrior = prior;
        }
        
    
        public String toString() {
            return data.toString();
        }
    
    }  // end of Node<T>
}
