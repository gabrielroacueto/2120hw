import junit.framework.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Assert;
import java.util.*;
public class LinkedListTest extends TestCase {
	LinkedList<String> testlist1;
	LinkedList<String> testlist2;
	LinkedList<String> testlist3;

	@Before
	public void setUp() {
		testlist1 = new LinkedList<String>();
		testlist2 = new LinkedList<String>();
		testlist3 = new LinkedList<String>();

		testlist1.add("String one");
		testlist1.add("String two");
		testlist1.add("String three");

		testlist2.add("String one");
		testlist2.add("String two");

		testlist3.add("String one");
	}

	public void testAdd(){
		assertEquals(testlist1.get(0), "String one");
		assertEquals(testlist2.get(1), "String two");
		assertEquals(testlist3.get(0), "String one");
		assertEquals(testlist1.get(2), "String three");

		testlist1.add("String three halves", 1);
		assertEquals(testlist1.get(1), "String three halves");

		testlist1.add("String five halves", 2);
		assertEquals(testlist1.get(2), "String five halves");

		testlist3.add("String two");
		assertEquals(testlist3.get(1), "String two");
	}

	public void testContains(){
		assertEquals(testlist1.contains("String one"), true);
		assertEquals(testlist2.contains("String three"), false);
		assertEquals(testlist3.contains(""), false);
	}

	public void testIndexOf(){
		assertEquals(testlist1.indexOf("String one"), 0);
		assertEquals(testlist1.indexOf("String two"), 1);
		assertEquals(testlist2.indexOf("This should not be here"), -1);
	}

	public void testIteratorAt(){
		Iterator iter1 = testlist1.iteratorAt("String one");
		assertEquals(iter1.next(), testlist1.get(0));
	}
}