import junit.framework.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Assert;
import java.util.*;

public class IteratorTest extends TestCase{

	//Declaring three different test lists of type string.
	private LinkedList<String> testList1;
	private LinkedList<String> testList2;
	private LinkedList<String> testList3;

	//
	private Iterator iter1;
	private Iterator iter2;
	private Iterator iter3;

	@Before
	public void setUp() {

		testList1 = new LinkedList<String>();
		testList1.add("String one");
		testList1.add("String two");
		testList1.add("String three");
		testList1.add("String four");
		
		testList2 = new LinkedList<String>();
		testList2.add("String one");

		testList3 = new LinkedList<String>();
		testList3.add("String one");
		testList3.add("String two");

		iter1 = new Iterator(testList1);
		iter2 = new Iterator(testList2);
		iter3 = new Iterator(testList3);
	}

	public void testHasNext(){
		assertEquals(iter1.hasNext(), true);
		assertEquals(iter2.hasNext(), false);
		assertEquals(iter2.hasNext(), false);
		assertEquals(iter3.hasNext(), true);
		String moveIter3 = (String)iter3.next();
		assertEquals(iter3.hasNext(), false);
	}

	public void testHasPrior(){
		assertEquals(iter1.hasPrior(), false);
		assertEquals(iter2.hasPrior(), false);
		String moveIter3 = (String)iter3.next();
		assertEquals(iter3.hasPrior(), true);
		String moveIter1 = (String)iter1.next();
		assertEquals(iter1.hasPrior(), true);
	}

	public void testNext(){
		assertEquals((String)iter1.next(), "String one");
		assertEquals((String)iter1.next(), "String two");
		assertEquals((String)iter1.next(), "String three");
		assertEquals((String)iter2.next(), "String one");
		assertEquals((String)iter3.next(), "String one");
	}

	public void testPrior(){
		String moveIter1 = (String)iter1.next();
		moveIter1 = (String)iter1.next();
		assertEquals((String)iter1.prior(), "String three");
		assertEquals((String)iter1.prior(), "String two");

		iter3.setToEnd();
		assertEquals((String)iter3.prior(), "String two");
		assertEquals((String)iter3.prior(), "String one");
	}

	public void testSetToEnd(){
		iter1.setToEnd();
		iter2.setToEnd();
		iter3.setToEnd();

		assertEquals((String)iter1.prior(), "String four");
		assertEquals((String)iter2.prior(), "String one");
		assertEquals((String)iter3.prior(), "String two");
	}

}